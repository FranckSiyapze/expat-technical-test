const url = "https://www.expat.com/dataset.php";

const vm = new Vue({
    el: '#app',
    data: {
      name:"",
      type:"",
      date:"",
      results: [],
      data:[]
    },
    mounted() {
      axios.get(url,{
        headers: {
            'Access-Control-Allow-Origin': 'http://localhost',
            'content-type':'application/json',
            'X-Requested-With':'XMLHttpRequest'
          }
      }).then(response => {
        this.data= response.data
        this.results = response.data
      },
      
      )
    },
    computed:{
      filterArticle: function(){
        if(this.name){
          //return this.results.filter(article => !article.title.indexOf(this.name))
          return this.results.filter(item => {
            return this.name
              .toLowerCase()
              .split(" ")
              .every(v => item.title.toLowerCase().includes(v));
          });
        }else if(this.type){
          return this.results.filter(item => {
            return this.type
              .toLowerCase()
              .split(" ")
              .every(v => item.type.toLowerCase().includes(v));
          });
        }else if(this.date){
          if(this.date =="asc"){
            this.results.sort( ( a, b) => {
                return new Date(a.date) - new Date(b.date);
            });
          }else if(this.date =="desc"){
            this.results.sort( ( a, b) => {
                return new Date(b.date) - new Date(a.date);
            });
          }
          return this.results;
        }else {
          return this.results
        }
        
    }
    }
  });